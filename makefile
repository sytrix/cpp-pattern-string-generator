
FILES_STRING_GENERATOR = \
	obj/string_generator/SGChar.o \
	obj/string_generator/SGElement.o \
	obj/string_generator/SGChoice.o \
	obj/string_generator/SGRollOut.o \
	obj/string_generator/SGString.o \
	obj/string_generator/SGToken.o \
	obj/string_generator/StringGenerator.o \
	obj/string_generator/StringPattern.o

COMPILER_OPTION = "-g"

build : bin/test_string_generator

buildobjdir :
	mkdir obj
	mkdir obj/string_generator

cleanobjdir : 
	rm -r obj

bin/test_string_generator : $(FILES_STRING_GENERATOR) obj/test_string_generator.o
	g++ $(COMPILER_OPTION) -o $@ $^

obj/string_generator/%.o : src/string_generator/%.cpp src/string_generator/%.hpp
	g++ $(COMPILER_OPTION) -o $@ -c $<

obj/test_string_generator.o : src/test_string_generator.cpp
	g++ $(COMPILER_OPTION) -o $@ -c $<