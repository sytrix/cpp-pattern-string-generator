# A Consistent String Generator

This project is a string generator that allow to make consistent generation from patterns.
The code is writen in C++, you can easily update it in order to put my code in your project.

---

## Project Testing
You can build the project and run the program in order to test string pattern

### Build

Clone this project and build it with make

```bash
git clone git@gitlab.com:sytrix/cpp-pattern-string-generator.git
cd cpp-pattern-string-generator
make
```

A binary will be generated in the `bin` directory.
You will be able to test pattern with it.

### Run it

Command arguments

```bash
./bin/test_string_generator <pattern> [nbGen]
```

Example of command with output 

```bash
$ ./bin/test_string_generator "[(bal)(gor)(gar)(vla)(bru)(ibu)(tek)(tol)(tal)]{2,4}" 10
brutekibubru
vlatol
brugor
vlabrugor
garvlabal
brutek
tolbrutek
gartektol
gortal
gorgor
```

---

## How Pattern Work 
You have differents operator in order to make a consistent string generation.
Pattern is made in order to match with regular expression (at some exception).

### Random choice

In order to make a random choice between characters, string or other pattern you can use this operator `[]` 
For example, this pattern `[abc]` will output one of the character `a`, `b` or `c` randomly
You can also specify string inside this operator as following `[(foo)(bar)]`, the result will be one of the two string `foo` or `bar`

### Repeat the operation

You can manage repetition by using `{}`
For example `Dh(o){6}!` will output `Dhoooooo!`
If you want to generate a random number of repetition you can specify two number, the computer will choose the number of repetition for you.
Example with 10 generation of the following pattern : `[01]{2,8}`
```bash
1000011
000
1010
01
1111110
1100
0000
10
101
1101
```

### To conclude

You can easily create a much more ellaborate consistent string generator by making pattern with the previous explanation.
You can easily create "Lorem ipsum" text or making name generator with viking style...