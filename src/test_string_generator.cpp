#include <iostream>
#include <vector>
#include <cmath>
#include "string_generator/StringGenerator.hpp"

void printListString(std::vector<std::string> listString) 
{
    int nbString = listString.size();

    for (int i = 0; i < nbString; i++) {
        std::cout << listString[i] << std::endl;
    }
}

int main(int argc, char *argv[]) 
{
    srand(time(0));

    if (argc == 2 || argc == 3) {

        int nbGen = 1;

        if (argc == 3) {
            nbGen = atoi(argv[2]);
        }


        printListString(StringGenerator::generateFromString(argv[1], nbGen));
    } else {
        std::cout << "Use this command with params" << std::endl;
        std::cout << argv[0] << " <pattern> [nbGen]" << std::endl;
    }

    //std::cout << "Generate name from string" << std::endl;
    //printListString(stringGenerator->generateFromString("[mnhtrpkl][(ian)(an)aaaaeeeeeiiiiou][mnhtrpkl][(ian)(an)aaaa(az)eeeeeiiii(ic)ou]", 10));  
    //printListString(stringGenerator->generateFromString("[(bal)(gor)(gar)(vla)(bru)(ibu)(tek)(tol)(tal)]{2,4}", 10));  
    //printListString(StringGenerator::generateFromString("([(gr)(tr)(br)(kr)ckkbjzttllhh][(ou)(an)aaaeeiiooo]){1,3}[flckrz(lt)][(us)(ium)(um)]", 50));
    


    return 0;
}

