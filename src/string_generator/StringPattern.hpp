#ifndef STRINGPATTERN_H
#define STRINGPATTERN_H

class SGRollOut;

class StringPattern
{
	public:

		StringPattern(std::string pattern);
		~StringPattern();


		std::string generateString();

		
	protected:
		
	private:
		SGRollOut *m_start;
};

#endif





