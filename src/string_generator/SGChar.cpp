#include "SGChar.hpp"



SGChar::SGChar(char c) : m_char(c) 
{
	
}

SGChar::~SGChar() 
{
	
}

std::string SGChar::getStringStruct(int paddingLeft)
{
    std::string result = "";
    for (int i = 0; i < paddingLeft; i++) {
        result += "\t";
    }

    result += "CHAR : \"" + std::string(1, m_char) + "\"";

    return result;
}

std::string SGChar::generateString()
{
    return std::string(1, m_char);
}
