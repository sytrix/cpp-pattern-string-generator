#include "SGRollOut.hpp"

#include "SGChoice.hpp"
#include "SGString.hpp"

#include "SGToken.hpp"

#include <iostream>
#include <cmath>

SGRollOut::SGRollOut(std::vector<SGToken *>::iterator *it, std::vector<SGToken *>::iterator end, bool isRoot) : SGElement::SGElement()
{
    SGElement *lastElement = NULL;

    while (*it != end) {
        SGToken *token = **it;
        ++*it;

        int type = token->getTokenType();

        if (type == SGToken::STRING_OPEN) {
            lastElement = new SGRollOut(it, end, false);
            m_elements.push_back(lastElement);
        } else if (type == SGToken::STRING_CLOSE) {    
            if (isRoot) {
                fprintf(stdout, "[ERROR SGRollOut] : can't close\n");
            }
            return;
        } else if (type == SGToken::CHOICE_OPEN) {
            lastElement = new SGChoice(it, end);
            m_elements.push_back(lastElement);
        } else if (type == SGToken::OPTION_OPEN) {
            if (lastElement == NULL) {
                fprintf(stdout, "[ERROR SGRollOut] : there is no element before the option\n");
            } else {
                std::vector<std::string> options = this->parseOptions(it, end);
                int nbOptions = options.size();
                if (nbOptions == 1) {
                    int nb = std::atoi(options[0].c_str());
                    lastElement->setMin(nb);
                    lastElement->setMax(nb);
                } else if (nbOptions == 2) {
                    lastElement->setMin(std::atoi(options[0].c_str()));
                    lastElement->setMax(std::atoi(options[1].c_str()));
                }
            }
        } else if (type == SGToken::STRING) {
            lastElement = new SGString(token->getString());
            m_elements.push_back(lastElement);
        } else {
            fprintf(stdout, "[ERROR SGRollOut] : token %s unexpected\n", SGToken::tokenToString(type).c_str());
        }
    };

    if (!isRoot) {
        fprintf(stdout, "[ERROR SGRollOut] : unexpected end\n");
    }
}

SGRollOut::~SGRollOut() {
	for (std::vector<SGElement *>::iterator it = m_elements.begin(); it != m_elements.end(); ++it) {
        delete *it;
    }
}

std::string SGRollOut::getStringStruct(int paddingLeft)
{
    std::string result = "";
    for (int i = 0; i < paddingLeft; i++) {
        result += "\t";
    }

    result += "ROLL_OUT : (" + std::to_string(m_elements.size()) + ")";
    for (std::vector<SGElement *>::iterator it = m_elements.begin(); it != m_elements.end(); ++it) {
        SGElement *element = *it;
        result += "\n" + element->getStringStruct(paddingLeft + 1);
    }

    return result;
}

std::string SGRollOut::generateString()
{
    std::string result = "";

    for (std::vector<SGElement *>::iterator it = m_elements.begin(); it != m_elements.end(); ++it) {
        SGElement *element = *it;

        int min = element->getMin();
        int max = element->getMax();
        int repetitionNumber = min + (rand() % (max - min + 1));
        for (int i = 0; i < repetitionNumber; ++i) {
            result += element->generateString();
        }
    }
    return result;
}