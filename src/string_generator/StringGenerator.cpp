#include <string>
#include <vector>
#include "StringGenerator.hpp"
#include "StringPattern.hpp"

std::vector<std::string> StringGenerator::generateFromString(std::string pattern, int nbGen)
{
    std::vector<std::string> listString;

    // parse the pattern only one time
    StringPattern stringPattern(pattern);

    // generate string without reparsing the pattern
    for (int i = 0; i < nbGen; i++) {
        listString.push_back(stringPattern.generateString());
    }

    // return the list of string generated
    return listString;
}

