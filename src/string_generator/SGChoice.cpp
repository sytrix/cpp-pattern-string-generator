#include "SGChoice.hpp"

#include "SGRollOut.hpp"
#include "SGString.hpp"
#include "SGChar.hpp"

#include "SGToken.hpp"

#include <iostream>
#include <cmath>

SGChoice::SGChoice(std::vector<SGToken *>::iterator *it, std::vector<SGToken *>::iterator end) : SGElement::SGElement()
{

    while (*it != end) {
        SGToken *token = **it;
        ++*it;

        int type = token->getTokenType();

        if (type == SGToken::STRING_OPEN) {
            SGRollOut *rollOut = new SGRollOut(it, end, false);
            m_choices.push_back(rollOut);
        } else if (type == SGToken::CHOICE_OPEN) {
            SGChoice *choice = new SGChoice(it, end);
            m_choices.push_back(choice);
        } else if (type == SGToken::CHOICE_CLOSE) {    
            return;
        } else if (type == SGToken::STRING) {
            std::string str = token->getString();
            for (std::string::iterator it = str.begin(); it != str.end(); ++it) {
                SGChar *string = new SGChar(*it);
                m_choices.push_back(string);
            }
        } else {
            fprintf(stdout, "[ERROR SGChoice] : token %s unexpected\n", SGToken::tokenToString(type).c_str());
        }
    };

    fprintf(stdout, "[ERROR SGChoice] : unexpected end\n");
}

SGChoice::~SGChoice() {
	for (std::vector<SGElement *>::iterator it = m_choices.begin(); it != m_choices.end(); ++it) {
        delete *it;
    }
}

std::string SGChoice::getStringStruct(int paddingLeft)
{
    std::string result = "";
    for (int i = 0; i < paddingLeft; i++) {
        result += "\t";
    }

    result += "CHOICE : (" + std::to_string(m_choices.size()) + ")";
    for (std::vector<SGElement *>::iterator it = m_choices.begin(); it != m_choices.end(); ++it) {
        SGElement *element = *it;
        result += "\n" + element->getStringStruct(paddingLeft + 1);
    }

    return result;
}

std::string SGChoice::generateString()
{
    int randomId = rand() % m_choices.size();
    SGElement *element = m_choices[randomId];
    return element->generateString();
}

