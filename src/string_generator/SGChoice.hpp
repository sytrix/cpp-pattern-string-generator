#ifndef SGCHOICE_H
#define SGCHOICE_H

#include "SGElement.hpp"
#include <vector>

class SGChoice : public SGElement
{
	public:
		SGChoice(std::vector<SGToken *>::iterator *it, std::vector<SGToken *>::iterator end);
		~SGChoice();

		// use for debug
		virtual std::string getStringStruct(int paddingLeft);
		virtual std::string generateString();
		
	protected:
		
	private:
		std::vector<SGElement *> m_choices;
};

#endif
