#ifndef SGTOKEN_H
#define SGTOKEN_H

class SGToken
{
	public:
		enum {
			UNEXPECTED = 0x0000, 
			STRING_OPEN = 0x0100, 
			STRING_CLOSE = 0x0101, 
			CHOICE_OPEN = 0x0102, 
			CHOICE_CLOSE = 0x0103, 
			OPTION_OPEN = 0x0104, 
			OPTION_CLOSE = 0x0105,
			OPTION_SEPARATOR = 0x0106,  
			STRING = 0x0200, 
		};

		static std::string tokenToString(int tokenType);



		SGToken(int tokenType);
		SGToken(int tokenType, std::string tokenStr);
		~SGToken();
		
		int getTokenType();

		char getChar();
		std::string getString();
		int getNumber();

	protected:
		
	private:
		int m_tokenType;
		std::string m_tokenStr; 
		
};

#endif
