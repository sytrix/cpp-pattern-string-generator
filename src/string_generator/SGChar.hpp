#ifndef SGCHAR_H
#define SGCHAR_H

#include "SGElement.hpp"

class SGChar : public SGElement
{
	public:
		SGChar(char c);
		~SGChar();
		
		// use for debug
		virtual std::string getStringStruct(int paddingLeft);
		virtual std::string generateString();
		
	protected:
		
	private:
		char m_char;
		
};

#endif
