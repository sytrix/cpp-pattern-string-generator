#include <string>
#include <vector>
#include <iostream>
#include "StringPattern.hpp"
#include "SGToken.hpp"
#include "SGRollOut.hpp"

// print debug to stdout
#define PRINT_DEBUG false

StringPattern::StringPattern(std::string pattern)
: m_start(NULL)
{
	std::vector<SGToken *> tokens;
    std::string tokenStr = "";
    bool escape = false;
    std::string::iterator it = pattern.begin();


    // transform pattern into several tokens 
    while (it != pattern.end()) {
        char c = *it;
        int tokenType = SGToken::UNEXPECTED;
        

        if (escape) {
            escape = false;
        } else {
            if (c == '(') {
                tokenType = SGToken::STRING_OPEN;
                ++it;
            } else if (c == ')') {
                tokenType = SGToken::STRING_CLOSE;
                ++it;
            } else if (c == '[') {
                tokenType = SGToken::CHOICE_OPEN;
                ++it;
            } else if (c == ']') {
                tokenType = SGToken::CHOICE_CLOSE;
                ++it;
            } else if (c == '{') {
                tokenType = SGToken::OPTION_OPEN;
                ++it;
            } else if (c == '}') {
                tokenType = SGToken::OPTION_CLOSE;
                ++it;
            } else if (c == ',') {
                tokenType = SGToken::OPTION_SEPARATOR;
                ++it;
            } else if (c == '\\') {
                escape = true;
                ++it;
            }
        }

        if (tokenType == SGToken::UNEXPECTED && !escape) {
            tokenStr += c;
            ++it;
        } else {
            if (!tokenStr.empty()) {
                tokens.push_back(new SGToken(SGToken::STRING, tokenStr));
                tokenStr = "";
            }
            tokens.push_back(new SGToken(tokenType, ""));
            
        }
    }
    if (!tokenStr.empty()) { // manage last few characters if needed
        tokens.push_back(new SGToken(SGToken::STRING, tokenStr));
    }

    
    // print tokens (debug)
    if (PRINT_DEBUG) {
        for (std::vector<SGToken *>::iterator it = tokens.begin(); it != tokens.end(); ++it)
        {
            SGToken *token = *it;
            std::cout << SGToken::tokenToString(token->getTokenType()) << " : \"" << token->getString() << "\"" << std::endl;
        }
    }

    // make data structure from tokens
    std::vector<SGToken *>::iterator tokenIterator = tokens.begin();
    m_start = new SGRollOut(&tokenIterator, tokens.end(), true);

    // delete tokens
    for (std::vector<SGToken *>::iterator it = tokens.begin(); it != tokens.end(); ++it)
    {
        SGToken *token = *it;
        delete token;
    }

    // print data structure (debug)
    if (PRINT_DEBUG) {
        std::cout << m_start->getStringStruct(0) << std::endl;
    }


}

StringPattern::~StringPattern() {
	delete m_start;
}

std::string StringPattern::generateString()
{
    return m_start->generateString();
}
