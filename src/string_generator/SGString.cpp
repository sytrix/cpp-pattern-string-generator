#include "SGString.hpp"



SGString::SGString(std::string string) : SGElement::SGElement(), m_string(string) 
{
	
}

SGString::~SGString() 
{
	
}

std::string SGString::getStringStruct(int paddingLeft)
{
    std::string result = "";
    for (int i = 0; i < paddingLeft; i++) {
        result += "\t";
    }

    result += "STR : \"" + m_string + "\"";

    return result;
}

std::string SGString::generateString()
{
    return m_string;
}