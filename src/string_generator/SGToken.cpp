#include <string>
#include "SGToken.hpp"


std::string SGToken::tokenToString(int tokenType)
{
    if (tokenType == UNEXPECTED) {
        return "UNEXPECTED";
    } else if (tokenType == STRING_OPEN) {
        return "STRING_OPEN";
    } else if (tokenType == STRING_CLOSE) {
        return "STRING_CLOSE";
    } else if (tokenType == CHOICE_OPEN) {
        return "CHOICE_OPEN";
    } else if (tokenType == CHOICE_CLOSE) {
        return "CHOICE_CLOSE";
    } else if (tokenType == OPTION_OPEN) {
        return "OPTION_OPEN";
    } else if (tokenType == OPTION_CLOSE) {
        return "OPTION_CLOSE";
    } else if (tokenType == OPTION_SEPARATOR) {
        return "OPTION_SEPARATOR";
    } else if (tokenType == STRING) {
        return "STRING";
    } else {
        return "ERROR";
    }
}

SGToken::SGToken(int tokenType)
: SGToken(tokenType, "")
{

}

SGToken::SGToken(int tokenType, std::string tokenStr)
: m_tokenType(tokenType), m_tokenStr(tokenStr)
{

}

SGToken::~SGToken() {
	
}

int SGToken::getTokenType()
{
    return m_tokenType;
}

char SGToken::getChar()
{
    if (m_tokenStr.size() == 1) {
        return m_tokenStr[0];
    }

    return 0;
}

std::string SGToken::getString()
{
    return m_tokenStr;
}

int SGToken::getNumber()
{
    return atoi(m_tokenStr.c_str());
}
