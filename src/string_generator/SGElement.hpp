#ifndef SGELEMENT_H
#define SGELEMENT_H

#include <string>
#include <vector>

class SGToken;

class SGElement
{
	public:
		SGElement();
		~SGElement();

		std::vector<std::string> parseOptions(std::vector<SGToken *>::iterator *it, std::vector<SGToken *>::iterator end);
		virtual std::string generateString();

		// use for debug
		virtual std::string getStringStruct(int paddingLeft);

		void setMin(int min);
		void setMax(int max);
		int getMin();
		int getMax();
		
	protected:
		

	private:
		int m_min;
		int m_max;
};

#endif
