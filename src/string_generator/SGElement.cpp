#include "SGElement.hpp"

#include "SGToken.hpp"

SGElement::SGElement() : m_min(1), m_max(1) {
	
}

SGElement::~SGElement() {
	
}

std::vector<std::string> SGElement::parseOptions(std::vector<SGToken *>::iterator *it, std::vector<SGToken *>::iterator end)
{
    std::vector<std::string> options;

    while (*it != end) {
        SGToken *token = **it;
        ++*it;

        int type = token->getTokenType();

        if (type == SGToken::OPTION_CLOSE) {
            return options;
        } else if (type == SGToken::STRING) {
            options.push_back(token->getString());

        } else if (type != SGToken::OPTION_SEPARATOR) {
            fprintf(stdout, "[ERROR SGElement] : token %s unexpected\n", SGToken::tokenToString(type).c_str());
        }
    };

    fprintf(stdout, "[ERROR SGElement] : unexpected end of options\n");
    return options;
}

std::string SGElement::generateString()
{
    // to override
    return "";
}

std::string SGElement::getStringStruct(int paddingLeft)
{
    // to override
    return "";
}

void SGElement::setMin(int min)
{
    m_min = min;
}

void SGElement::setMax(int max)
{
    m_max = max;
}

int SGElement::getMin()
{
    return m_min;
}

int SGElement::getMax()
{
    return m_max;
}

