#ifndef SGROLLOUT_H
#define SGROLLOUT_H

#include "SGElement.hpp"
#include <vector>

class SGRollOut : public SGElement
{
	public:
		SGRollOut(std::vector<SGToken *>::iterator *it, std::vector<SGToken *>::iterator end, bool isRoot);
		~SGRollOut();

		// use for debug
		virtual std::string getStringStruct(int paddingLeft);

		virtual std::string generateString();
		
	protected:
		
	private:
		std::vector<SGElement *> m_elements;
		

		
};

#endif
