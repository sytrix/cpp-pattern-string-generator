#ifndef SGSTRING_H
#define SGSTRING_H

#include "SGElement.hpp"

class SGString : public SGElement
{
	public:
		SGString(std::string string);
		~SGString();

		// use for debug
		virtual std::string getStringStruct(int paddingLeft);
		virtual std::string generateString();
		
	protected:
		
	private:
		std::string m_string;
};

#endif
